FROM ruby:2.7.0

WORKDIR /app

# copied these files to be used by package managaers bundler and yarn to install gems and node-modules
COPY Gemfile Gemfile.lock package.json yarn.lock /app/

# used to add yarn source repository to apt source list to install yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list

RUN apt-get update

# used to get installation script for correct version of nodeJS
RUN curl -sL https://deb.nodesource.com/setup_16.x -o /tmp/nodesource_setup.sh
RUN bash /tmp/nodesource_setup.sh


RUN apt-get install -y yarn nodejs default-libmysqlclient-dev

# installing specific version of bundler as per Gemfile.lock file
RUN gem install bundler -v 2.4.7

# installing all gems and node-modules
RUN bundle install
RUN yarn install

# installing rails (redundant)
# RUN gem install rails

# copying the source code for the app to the docker container
COPY . /app/
EXPOSE 3000
ENTRYPOINT [ "./entrypoint.sh" ]
