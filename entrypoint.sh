#!/bin/sh

set -e
bundle exec rails server -b 0.0.0.0    # command to start the rails server and bind it to 0.0.0.0
bundle exec rails db:create
bundle exec rails db:migrate
exec "$@"