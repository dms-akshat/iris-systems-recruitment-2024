# IRIS Systems Recruitment 2024

## Tasks Implemented

- [Task 1](https://gitlab.com/dms-akshat/iris-systems-recruitment-2024/-/tree/task1?ref_type=heads) Packed the Ruby on Rails Application in a Docker Image (bugged [1](#database-create-error))
- [Task 2](https://gitlab.com/dms-akshat/iris-systems-recruitment-2024/-/tree/task2?ref_type=heads) Connected app container to seperate database container
- [Task 3](https://gitlab.com/dms-akshat/iris-systems-recruitment-2024/-/tree/task3?ref_type=heads) Implemented Nginx reverse proxy (bugged [2](#reverse-proxy-error))
- [Task 4](https://gitlab.com/dms-akshat/iris-systems-recruitment-2024/-/tree/task4?ref_type=heads) Connected multiple instances of app to nginx and database
- [Task 5](https://gitlab.com/dms-akshat/iris-systems-recruitment-2024/-/tree/task5?ref_type=heads) Enabled persistance for database and nginx config
- [Task 6](https://gitlab.com/dms-akshat/iris-systems-recruitment-2024/-/tree/task6?ref_type=heads) Implemented Docker Compose to bring up all containers with a single command
- [Task 7](https://gitlab.com/dms-akshat/iris-systems-recruitment-2024/-/tree/task7?ref_type=heads) Implemented Rate Limiting through Nginx
- [Bonus Task](https://gitlab.com/dms-akshat/iris-systems-recruitment-2024/-/tree/bonus_CI/CD?ref_type=heads) Implemented CI/CD pipeline (bugged [3](#cicd-pipeline-error))

##### Rough Thought Process for all tasks is written in [doc.txt](https://gitlab.com/dms-akshat/iris-systems-recruitment-2024/-/blob/main/Doc.txt?ref_type=heads)

## Things to keep in mind before running

- A directory named `database` needs to be present in the root directory, if not present, create using :-
```bash
$ mkdir database
```

## App Showcase

#### Docker Compose output present in [log.txt](https://gitlab.com/dms-akshat/iris-systems-recruitment-2024/-/blob/main/log.txt?ref_type=heads) 

#### Home Page:-
<img src="./assets/home.jpg" alt="Ruby error page for database connection error">

#### Home Page:-
<img src="./assets/tasks.jpg" alt="Ruby error page for database connection error">

#### Rate Limiting:-
Tested by setting rate limit to 2 requests/second with the followinng command:-

```bash
$ for i in {1..20}; do curl -I http://localhost:8080; done
```
<img src="./assets/rate_limit_test.png" alt="Test Result for Rate Limiting">

#### CI/CD Pipeline:-
CI/CD Pipeline required variables
<img src="./assets/gitlab_variables.jpg" alt="List of assigned variables for CI/CD Pipeline">

<img src="./assets/pipeline_output.png" alt="List of assigned variables for CI/CD Pipeline">

## Issues Faced

#### Database Connection Error
<img src="./assets/db_conn_err.jpg" alt="Ruby error page for database connection error">

Description:-
The app container couldn't connect to the database container.

Fix:-
Setup database host according to docker networks. Use the Database service name to DNS Query inside the docker network and connect to Database container using TCP/IP Socket instead of UNIX Socket
     
#### Database Create Error
<img src="./assets/db_create_db.jpg" alt="Ruby error page for database create error">

Description:-
The container doesn't create the database automatically, so "Database not found" error page is received.

Temp_Fix:-
`Create Database` button needs to be pressed manually when for the first run.
Tried Creating a health check to fix but didn't work

#### Database Migrate Error
<img src="./assets/db_run_migrations.jpg" alt="Ruby error page for database migrate error">

Description:-
The container doesn't migrate the database automatically, so "Pending Migrations" error page is received.

Temp_Fix:-
`Run Pending Migrations` button needs to be pressed manually when for the first run.
Tried Creating a health check to fix but didn't work

#### Database Test_DB Error
<img src="./assets/db_test_db_err.jpg" alt="Ruby error page for test database create error">

Description:-
MySQL syntax error is observed when running app due to sqlite test db create code.

Temp_Fix:-
Commented out the test db creation part
<b>NOTE: This is not the correct fix</b>

#### Database Login Error
<img src="./assets/db_user_cred_err.jpg" alt="Database connection error due to improper user credentials">

Description:-
The app was unable to connect to Database due to credential error

Fix:-
Passed the credentials as Environment Variables through `docker-compose.yml`

#### Reverse Proxy Error
<img src="./assets/rev_proxy_err.jpg" alt="Invalid Authenticity Token Error">

Description:-
The nginx container is unable to process GET requests properly so images are not visible

Fix:-
(Still left to figure out)

#### CI/CD Pipeline Error
Error:-
Cannot perform an interactive login from a non TTY device

Description:-
The pipeline is unable to run due to docker login error

Fix:-
The variable set in gitlab settings weren't being injected due to them being protected
Changed the property to allow injection